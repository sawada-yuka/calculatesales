package jp.alhinc.sawada_yuka.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		HashMap<String, String> branchmap = new HashMap<String, String>();
		HashMap<String, Long> salesmap = new HashMap<String, Long>();

		//支店定義ファイル読み込み
		if(!Reader(args[0],"branch.lst",branchmap,salesmap)) {
			return;
		}

		List<File> rcdlist = new ArrayList<File>();

		File dir = new File(args[0]);
		File[] list = dir .listFiles();

		for(int i = 0; i < list.length; i++) {
			String str = list[i].getName();
			if(str.matches("[0-9]{8}.rcd") && list[i].isFile()) {
				rcdlist.add(list[i]);
			}
		}

		for(int i = 0; i < rcdlist.size()-1 ; i++) {
			String S = rcdlist.get(i).getName().substring(0, 8);
			String S2 = rcdlist.get(i+1).getName().substring(0, 8);
			int n = Integer.parseInt(S);
			int n2 = Integer.parseInt(S2);
			if((n + 1) != n2) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for(int j = 0; j < rcdlist.size(); j++) {
			BufferedReader br2 = null;

			try {
				FileReader fr2 = new FileReader(rcdlist.get(j));
				br2 = new BufferedReader(fr2);
				String line;
				List<String> saleslist = new ArrayList<String>();

				while((line = br2.readLine()) !=null) {
					saleslist.add(line);
				}
				if(!branchmap.containsKey(saleslist.get(0))) {
					System.out.println(rcdlist.get(j).getName() + "の支店コードが不正です");
					return;
				}
				if(saleslist.size() != 2) {
					System.out.println(rcdlist.get(j).getName() + "のフォーマットが不正です");
					return;
				}
				if(!saleslist.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long amount = salesmap.get(saleslist.get(0)) + Long.parseLong(saleslist.get(1));

				if(amount > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				salesmap.put(saleslist.get(0),amount);

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally {
				if(br2 != null) {
					try {
						br2.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}

		//集計結果出力
		if(!Writer(args[0],"branch.out",branchmap,salesmap)) {
			return;
		}
	}

	//支店定義ファイル読み込みの戻り値を返すメソッド
	public static boolean Reader(String path, String name, HashMap<String, String> branchmap, HashMap<String, Long> salesmap) {

		BufferedReader br = null;
		try {
			File file = new File(path,name);
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			while((line = br.readLine()) !=null) {
				String[] codename= line.split(",",0);

				if(codename.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				if(!codename[0].matches("^\\d{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				branchmap.put(codename[0],codename[1]);
				salesmap.put(codename[0],0L);
			}
			return true;
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
	}

	//集計結果出力の戻り値を返すメソッド
	public static boolean Writer(String path, String name, HashMap<String, String> branchmap, HashMap<String, Long> salesmap) {
		try {
			File file2 = new File(path,name);
			if (file2.createNewFile()) {
			}
			FileWriter fw = new FileWriter(file2);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);

			for(String key : branchmap.keySet()) {
			String str = key + "," + branchmap.get(key) + "," + salesmap.get(key);
				pw.println(str);
			}
			pw.close();
			return true;

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
	}
}